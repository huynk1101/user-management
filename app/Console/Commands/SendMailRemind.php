<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class SendMailRemind extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'remind:send_mail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send mail everyday';

    protected $candidateService;

    /**
     * Create a new command instance.
     *
     * ApproveRequestReceiveMoneyRemind constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            DB::table('users')->insert([
                'name' => 'HuyNK',
                'email' => 'huynk@rikkeisoft.com',
                'password' => Hash::make('password'),//password
            ]);
//            $email = new SendWelcomeEmail();
//            $this->dispatch($email);
            $this->info('Insert success!');
        } catch (\Exception $e) {
            Log::error(self::class.':'.__FUNCTION__, ['error' => $e->getMessage()]);
        }
    }
}
